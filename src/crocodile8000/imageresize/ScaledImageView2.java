package crocodile8000.imageresize;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;




/**
 * Более компактный вариант ScaledImageView за счет ScaleGestureDetector,
 * Спасибо Алексею Пушкареву
 * 
 * @author Crocodile8000
 *
 */
public class ScaledImageView2 extends View{
	
//	private static final String log = "ScaledImageView";
	
	private Bitmap img;
	private Rect imgDscRect;
	private int w,h;
	private int imgW, imgH, x2, y2, xCenter, yCenter;
	private float imgRatio;
	private int xpos , ypos ;
	private int pointerCount, pointerCountLast;
	private float maxScale = 10f, minScale = 0.2f;
	private boolean 
			draggable = true, 
			smartBounds = true, 
			scaleIfLess=false;
	
	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactor;

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		public boolean onScale(ScaleGestureDetector detector) {
		    mScaleFactor *= detector.getScaleFactor();
		    // Don't let the object get too small or too large.
		    mScaleFactor = Math.max(minScale, Math.min(mScaleFactor, maxScale));
		    invalidate();
		    return true;
		}
	}

	
	
	/**
	 * Установить картинку
	 * @param img
	 */
	public void setImg(Bitmap img){
		this.img = img;
		imgDscRect = null;
		invalidate();
	}

	
	/**
	 * Установить максимальное допустимое увеличение
	 * @param max
	 */
	public void setMaxScale(float max){
		if(max<1)max=1f;
		maxScale = max;
	}

	
	/**
	 * Установить минимальное допустимое уменьшение
	 * @param max
	 */
	public void setMinScale(float min){
		if(min>1)min=1f;
		minScale = min;
	}
	
	
	/**
	 * Задать возможность перемещения изображения
	 * @param draggable - можно или нельзя
	 */
	public void setDraggable(boolean draggable){
		this.draggable = draggable;
	}
	
	
	/**
	 * Задать более строгий ограничитель границ движения
	 * @param draggable - можно или нельзя
	 */
	public void setSmartBounds(boolean smartBounds){
		this.smartBounds = smartBounds;
	}
	
	
	/**
	 * Нужна или нет подгонка под экран если изображение меньше экрана
	 * (поставьте false если хотите избержать растягивания изображения
	 * @param
	 */
	public void setScaleIfLess(boolean needScaleIfLess){
		this.scaleIfLess = needScaleIfLess;
	}
	
	
	/**
	 * При изменении ориентации и др. событиях сбрасываем изображение в начальный вид
	 */
	protected void onConfigurationChanged (Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		imgDscRect = null;
		invalidate();
	}
	
	
	
	
	
	
	public ScaledImageView2(Context context) {
		super(context);
		init(context);}
	public ScaledImageView2(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);}
	public ScaledImageView2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context); }
	
	private void init(Context context){
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener()); }
	
	
	
	
	
	
	
	/**
	 * Цикл отрисовки
	 */
	public void onDraw(Canvas canv){
		if(img!=null) {
			if(imgDscRect == null) setStartImgRect();
			canv.drawBitmap(img, null, imgDscRect, null);
			calcScaleAndPosition();
		}
	}
	
	
	
	/**
	 * Задать начальное вписыывание изображения в рамки View
	 */
	private void setStartImgRect(){
		w = getWidth();
		h = getHeight();
		xCenter = w/2;
		yCenter = h/2;

		imgW = img.getWidth();
		imgH = img.getHeight();
		imgRatio = (float)imgW/(float)imgH;
		
		float scaleW, scaleH;
		scaleW = (float)w/(float)imgW;
		scaleH = (float)h/(float)imgH;
		
		// если ну нужно обазятельно увеличение под размер экрана и изображение меньше экрана
		if(!scaleIfLess && imgW<w && imgH<h){
			x2 = imgW;
			y2 = imgH; }
		// если нужна подгонка под экран даже если изображение меньше
		else{
			if(imgW>imgH){
				x2 = (int) (imgW*scaleW);
				y2 = (int) (x2/imgRatio);
			}else if(imgH>imgW){
				y2 = (int) (imgH*scaleH);
				x2 = (int) (y2*imgRatio); }
			// если изображение квадратное
			else{
				if(w>h)
					y2 = (int) (imgH*scaleH);
				else
					y2 = (int) (imgH*scaleW);
				x2 = y2; }
			// дополнительная проверка, вдруг вышли за рамки
			if(x2>w){
				x2 = (int) ((float)x2*((float)w/(float)x2));
				y2 = (int) ((float)y2*((float)w/(float)x2));
			}
			if(y2>h){
				x2 = (int) ((float)x2*((float)h/(float)y2));
				y2 = (int) ((float)y2*((float)h/(float)y2));
			}
		}
		
		mScaleFactor = 1f;
		
		imgDscRect = new Rect(xCenter-x2/2, yCenter-y2/2, xCenter+x2/2, yCenter+y2/2);
	}
	
	
	
	
	
	
	/**
	 * Установить размер Rect для изображения в соответствии с текущим масштабом
	 */
	private void calcScaleAndPosition(){
		
		// ограничение координат изображения рамками экрана
		if(draggable){
			// более свободные ограничители движения
			if(!smartBounds){
				if(xCenter<0)xCenter=0;
				else if(xCenter>w) xCenter=w;
				if(yCenter<0)yCenter=0;
				else if(yCenter>h) yCenter=h; }
			// более умные ограничители движения 
			// (что бы нельзя было утащить до черного фона если текущий масштаб позволяет вписать)
			else{
				if(x2*mScaleFactor <= w+1){
					xCenter = w/2;
				}else{
					if(xCenter>x2/2*mScaleFactor)xCenter=(int) (x2/2*mScaleFactor);
					else if(xCenter<w-(x2/2*mScaleFactor)) xCenter=(int) (w-(x2/2*mScaleFactor));
				}
				if(y2*mScaleFactor <= h+1){
					yCenter = h/2;
				}else{
					if(yCenter>y2/2*mScaleFactor)yCenter=(int) (y2/2*mScaleFactor);
					else if(yCenter<h-(y2/2*mScaleFactor)) yCenter=(int) (h-(y2/2*mScaleFactor)); }
				}
		}
		
		// усановка размеров квадрата отрисовки изображения 
		imgDscRect.set(
				(int)(xCenter - (x2/2)*mScaleFactor), 
				(int)(yCenter - (y2/2)*mScaleFactor), 
				(int)(xCenter + (x2/2)*mScaleFactor), 
				(int)(yCenter + (y2/2)*mScaleFactor)
				);
	}
		

	
	
	
	/**
	 * Стандартный обработчик касаний
	 */
	public boolean onTouchEvent(MotionEvent me) {
		pointerCount = me.getPointerCount();
		
		boolean needReset=false;
		if (pointerCount==1 && pointerCount < pointerCountLast ) {
			// с гарантией отследить когда подняли один палец и остался еще один, 
			// для того что бы не было скачка в перемещении
			needReset = true;
		}
		if (pointerCount == 1 && me.getAction() == MotionEvent.ACTION_DOWN) {
			xpos = (int) me.getX();
			ypos = (int) me.getY();
		}
		if (pointerCount == 1 && me.getAction() == MotionEvent.ACTION_MOVE){
			if ( !needReset && draggable){
				xCenter -= (int) (xpos - me.getX());
				yCenter -= (int) (ypos - me.getY()); }
			xpos = (int) me.getX();
			ypos = (int) me.getY(); 
		}
		
		mScaleDetector.onTouchEvent(me);

	    pointerCountLast = pointerCount;
	    invalidate();
		return true;
	}

}
