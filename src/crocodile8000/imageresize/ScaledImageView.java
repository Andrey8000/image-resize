package crocodile8000.imageresize;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


/**
 * ��������� ����������� �����������, ��������� �������, 
 * � ����� �� �������� � ��������������� (���� �� ���������)
 * 
 * @author Crocodile8000
 *
 */
public class ScaledImageView extends View{
	
//	private static final String log = "ScaledImageView";
	
	private Bitmap img;
	private Rect imgDscRect;
	private int w,h;
	private int imgW, imgH, x2, y2, xCenter, yCenter;
	private float imgRatio;
	private int pointerCount, pointerCountLast;
	private int[] xpos = new int[20], ypos = new int[20];
	private float touchLenCurr, touchLenStart, currScale, lastScale;
	private float maxScale = 10f, minScale = 0.2f;
	private boolean 
			needSaveStart = true, 
			draggable = true, 
			smartBounds = true, 
			scaleIfLess=false;
	
	
	
	/**
	 * ���������� ��������
	 * @param img
	 */
	public void setImg(Bitmap img){
		this.img = img;
		imgDscRect = null;
		invalidate();
	}

	
	/**
	 * ���������� ������������ ���������� ����������
	 * @param max
	 */
	public void setMaxScale(float max){
		if(max<1)max=1f;
		maxScale = max;
	}

	
	/**
	 * ���������� ����������� ���������� ����������
	 * @param max
	 */
	public void setMinScale(float min){
		if(min>1)min=1f;
		minScale = min;
	}
	
	
	/**
	 * ������ ����������� ����������� �����������
	 * @param draggable - ����� ��� ������
	 */
	public void setDraggable(boolean draggable){
		this.draggable = draggable;
	}
	
	
	/**
	 * ������ ����� ������� ������������ ������ ��������
	 * @param draggable - ����� ��� ������
	 */
	public void setSmartBounds(boolean smartBounds){
		this.smartBounds = smartBounds;
	}
	
	
	/**
	 * ����� ��� ��� �������� ��� ����� ���� ����������� ������ ������
	 * (��������� false ���� ������ ��������� ������������ �����������
	 * @param
	 */
	public void setScaleIfLess(boolean needScaleIfLess){
		this.scaleIfLess = needScaleIfLess;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * ��� ��������� ���������� � ��. �������� ���������� ����������� � ��������� ���
	 */
	protected void onConfigurationChanged (Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		imgDscRect = null;
		invalidate();
	}
	
	
	
	
	public ScaledImageView(Context context) {
		super(context);
	}
	public ScaledImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public ScaledImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	
	
	/**
	 * ���� ���������
	 */
	public void onDraw(Canvas canv){
		if(img!=null) {
			if(imgDscRect == null) setStartImgRect();
			canv.drawBitmap(img, null, imgDscRect, null);
			calcScaleAndPosition();
		}
	}
	
	
	
	/**
	 * ������ ��������� ����������� ����������� � ����� View
	 */
	private void setStartImgRect(){
		w = getWidth();
		h = getHeight();
		xCenter = w/2;
		yCenter = h/2;

		imgW = img.getWidth();
		imgH = img.getHeight();
		imgRatio = (float)imgW/(float)imgH;
		
		float scaleW, scaleH;
		scaleW = (float)w/(float)imgW;
		scaleH = (float)h/(float)imgH;
		
		// ���� �� ����� ����������� ���������� ��� ������ ������ � ����������� ������ ������
		if(!scaleIfLess && imgW<w && imgH<h){
			x2 = imgW;
			y2 = imgH; }
		// ���� ����� �������� ��� ����� ���� ���� ����������� ������
		else{
			if(imgW>imgH){
				x2 = (int) (imgW*scaleW);
				y2 = (int) (x2/imgRatio);
			}else if(imgH>imgW){
				y2 = (int) (imgH*scaleH);
				x2 = (int) (y2*imgRatio); }
			// ���� ����������� ����������
			else{
				if(w>h)
					y2 = (int) (imgH*scaleH);
				else
					y2 = (int) (imgH*scaleW);
				x2 = y2; }
			// �������������� ��������, ����� ����� �� �����
			if(x2>w){
				x2 = (int) ((float)x2*((float)w/(float)x2));
				y2 = (int) ((float)y2*((float)w/(float)x2));
			}
			if(y2>h){
				x2 = (int) ((float)x2*((float)h/(float)y2));
				y2 = (int) ((float)y2*((float)h/(float)y2));
			}
		}
		
		currScale = 1f;
		lastScale = 1f;
		imgDscRect = new Rect(xCenter-x2/2, yCenter-y2/2, xCenter+x2/2, yCenter+y2/2);
	}
	
	
	
	
	/**
	 * ���������� ������ Rect ��� ����������� � ������������ � currScale
	 */
	private void calcScaleAndPosition(){
		
		// ����������� ��������� ������ ����������� ������� ������
		if(draggable){
			// ����� ��������� ������������ ��������
			if(!smartBounds){
				if(xCenter<0)xCenter=0;
				else if(xCenter>w) xCenter=w;
				if(yCenter<0)yCenter=0;
				else if(yCenter>h) yCenter=h; }
			// ����� ����� ������������ �������� 
			// (��� �� ������ ���� ������� �� ������� ���� ���� ������� ������� ��������� �������)
			else{
				if(x2*currScale <= w+1){
					xCenter = w/2;
				}else{
					if(xCenter>x2/2*currScale)xCenter=(int) (x2/2*currScale);
					else if(xCenter<w-(x2/2*currScale)) xCenter=(int) (w-(x2/2*currScale));
				}
				if(y2*currScale <= h+1){
					yCenter = h/2;
				}else{
					if(yCenter>y2/2*currScale)yCenter=(int) (y2/2*currScale);
					else if(yCenter<h-(y2/2*currScale)) yCenter=(int) (h-(y2/2*currScale)); }
				}
		}
		
		// �������� �������� �������� ��������� ����������� 
		imgDscRect.set(
				(int)(xCenter - (x2/2)*currScale), 
				(int)(yCenter - (y2/2)*currScale), 
				(int)(xCenter + (x2/2)*currScale), 
				(int)(yCenter + (y2/2)*currScale)
				);
	}
		
	
	/**
	 * ��������� ���������� ����� ��������� ��� ������ 2� ������������� �������
	 */
	private void calcStartLen(){
		touchLenStart = android.util.FloatMath.sqrt( (ypos[0] -ypos[1])*(ypos[0] -ypos[1]) + 
				(xpos[0] -xpos[1])*(xpos[0] -xpos[1]));
		touchLenCurr = touchLenStart;
		if(touchLenStart> w/6) {
			needSaveStart = false;
			lastScale = currScale;
		}
//		Log.d(log, "lastScale: "+lastScale);
	}
	
	
	/**
	 * ������������ ��������� ���������� ����� ������� ������� ��� ��������
	 */
	private void calcCurrLen(){
		if(needSaveStart) calcStartLen();
		touchLenCurr = android.util.FloatMath.sqrt( (ypos[0] -ypos[1])*(ypos[0] -ypos[1]) + 
				(xpos[0] -xpos[1])*(xpos[0] -xpos[1]));
		currScale = lastScale * (touchLenCurr / touchLenStart);
		correctionScale();
//		Log.i(log, "currScale: "+ "  "+currScale );
	}
	
	
	/**
	 * ������� ��������������� �����������
	 */
	private void correctionScale(){
		if(currScale< minScale) currScale = minScale ;
		else if (currScale> maxScale)currScale = maxScale;
		if(lastScale< minScale)lastScale = minScale ;
		else if (lastScale> maxScale)lastScale = maxScale;
	}
	
	
	/**
	 * ����������� ���������� �������
	 */
	public boolean onTouchEvent(MotionEvent me) {
		pointerCount = me.getPointerCount();
		
		boolean needReset=false;
		if (pointerCount==1 && pointerCount < pointerCountLast ) {
			// � ��������� ��������� ����� ������� ���� ����� � ������� ��� ����, ��� ���� ��� �� �� ���� ������ � �����������
			needReset = true;
		}
		
	    for(int i=0; i<pointerCount; i++) {
				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					xpos[i] = (int) me.getX(i);
					ypos[i] = (int) me.getY(i);
				}
		
				if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if (pointerCount==1 && !needReset && draggable){
						xCenter -= (int) (xpos[i] - me.getX(i));
						yCenter -= (int) (ypos[i] - me.getY(i));
					}
					xpos[i] = (int) me.getX(i);
					ypos[i] = (int) me.getY(i);
					if(pointerCount==2 && i==1) calcCurrLen();
				}

				if (me.getAction() == MotionEvent.ACTION_UP || me.getAction() == MotionEvent.ACTION_CANCEL) {
					if(pointerCount<2){
						needSaveStart = true;
					}
				}
	    }
	    pointerCountLast = pointerCount;
	    invalidate();
		return true;
	}

}
