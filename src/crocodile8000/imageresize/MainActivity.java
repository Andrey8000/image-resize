package crocodile8000.imageresize;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.Window;

public class MainActivity extends Activity {
	
	Bitmap img;
	Random rnd;
	ScaledImageView2 scim;
	int lastR;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lay_main);
		
		rnd = new Random();
		generateNewImage();
		
		// ������� ScaledImageView �� layout-�
		scim = (ScaledImageView2) findViewById(R.id.scaledImageView1);
		// ������ �����������
		scim.setImg(img);
		
		// �������������� ���������
		scim.setMaxScale(4f);
		scim.setMinScale(1f);
		scim.setDraggable(true);
		scim.setSmartBounds(true);
		scim.setScaleIfLess(false);
	}

	
	/**
	 * ������� ������ ���������� �����������
	 * @param v
	 */
	public void updImg(View v){
		generateNewImage();
		scim.setImg(img);
	}

	
	/**
	 * ����� ������ �����������
	 */
	private void generateNewImage(){
		int r = rnd.nextInt(5);
		while(lastR == r){r = rnd.nextInt(5);}
		if(r==0) img = BitmapFactory.decodeResource(getResources(), R.drawable.desert);
		else if(r==1) img = BitmapFactory.decodeResource(getResources(), R.drawable.penguins);
		else if(r==2) img = BitmapFactory.decodeResource(getResources(), R.drawable.tulips);
		else if(r==3) img = BitmapFactory.decodeResource(getResources(), R.drawable.rect100);
		else if(r==4) img = BitmapFactory.decodeResource(getResources(), R.drawable.rect500);
		lastR = r;
	}
	
	
}
